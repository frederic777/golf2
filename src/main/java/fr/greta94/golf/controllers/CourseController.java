package fr.greta94.golf.controllers;

import fr.greta94.golf.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class CourseController {
    @Autowired
    private GolfRepository golfRepository;
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private HoleRepository holeRepository;

    @RequestMapping(value = "/courses/{id}", method = RequestMethod.POST)
    public String Courses(Model model, @PathVariable("id") long id) {
        model.addAttribute("courses", courseRepository.findById(id));

        return "courses";
    }

    @RequestMapping(value = "/courseDelete/{id}", method = RequestMethod.GET)
    public String courseDelete(Model model, @PathVariable("id") Long id) {

        Course course = courseRepository.getOne(id);
        model.addAttribute("hole", holeRepository.findAllByCourse(course));
        model.addAttribute("course", course);

        return "courses";
    }
    @RequestMapping(value = "/courseAdd/{id}", method = RequestMethod.GET)
    public String course(Model model, @PathVariable("id") Long id, @RequestParam("nomParc") String nomParc,
                         @RequestParam("nbrTrou") long nbrTrou) {

        Golf golf1 = golfRepository.getOne(id);
        List<Course> courses = courseRepository.findAllByGolf(golf1);
        Course nouveaucourse=new Course(nomParc);
        for(int i=0;i<nbrTrou;i++) {
            Hole hole1=new Hole();
            hole1.setNum(i+1);
            nouveaucourse.addHoles(hole1);

        }
//        holeRepository.saveAll(nouveaucourse.getHoles());
        courses.add(nouveaucourse);
        courseRepository.saveAll(courses);
//        golfRepository.save(golf1);
        for (Course course : golf1.getCourses()) {
            int temps = 0;
            int num = 1;

            int heures = 0;
            int minutes = 0;
            String horaireSplit[]={"",""};
            int horaireInt2=0;
            int horaireInt=0;
            if (course.getHoles()!=null){
        for (Hole hole : course.getHoles()) {


                hole.setNum(Integer.parseInt(String.format("%02d", num)));
                int par = hole.getPar();
                if (par == 3 && "3".equals("3")) {
                    temps += 11;
                } else if (par == 4 && "3".equals("3")) {
                    temps += 14;
                } else if (par == 5 && "3".equals("3")) {
                    temps += 17;
                } else if (par == 3) {
                    temps += 9;
                } else if (par == 4) {
                    temps += 12;
                } else {
                    temps += 14;
                }
                heures = temps / 60;
                minutes = temps % 60;
//                if ((horaire == null) ||(horaire.equalsIgnoreCase("")))  {
//                    horaire = "00:00";
//                }
//                if(course.getId()==courseId) {
//                    horaireSplit = (horaire.split(":"));
//                    horaireInt = Integer.parseInt(horaireSplit[0]);
//                    horaireInt = (horaireInt + heures) % 24;
//                    horaireInt2 = Integer.parseInt(horaireSplit[1]);
//                    horaireInt2 =(horaireInt2+ minutes)%60;
//                    hole.setHoraire((String.format("%02d:%02d", horaireInt, horaireInt2)));
//                }
             hole.setWalkTime((String.format("%02d:%02d", heures, minutes)));

                num++;
            }
        }}
        model.addAttribute("courses", courses);
        model.addAttribute("golf", golf1);
        return "golf";
    }


    @RequestMapping(value = "/courseEdit/{idGolf}", method = RequestMethod.GET)
    public String modifyCourse(Model model, @PathVariable("idGolf") Long id, @RequestParam("nomParc") String nomParc
            ,@RequestParam("nbrTrou") int nbrTrou) {

        Golf golf1=golfRepository.findById(id).get();
       Course course =new Course();
       course.setName(nomParc);
       golf1.addCourses(course);

        courseRepository.save(course);
        golf1.addCourses(course);
        golfRepository.save(golf1);
        model.addAttribute("hole", holeRepository.findAllByCourse(course));
        model.addAttribute("courses", golf1.getCourses());
        model.addAttribute("golf", golf1);
        model.addAttribute("nbrTrou",nbrTrou);

        return "golfEdit";
    }
}

