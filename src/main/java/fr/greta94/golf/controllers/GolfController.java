package fr.greta94.golf.controllers;

import fr.greta94.golf.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class GolfController {
    @Autowired
    private GolfRepository golfRepository;

    @Autowired
    private CourseRepository courseRepository;

    @RequestMapping("/golfs")
    public String golfs(Model model) {
        model.addAttribute("golfs", golfRepository.findAll());

        return "golfs";
    }

    @RequestMapping(value = "/golfAdd", method = RequestMethod.GET)
    public String golfsAdd() {
        return "golfsAdd";
    }

    @RequestMapping(value = "/golfsAdd", method = RequestMethod.POST)
    public String golfsAdd(Model model, @RequestParam("nouvGolf") String nouvGolf) {
        Golf golf1 = new Golf();
        long ngId = (golfRepository.findAll().size() + 1);
        golf1.setId(ngId);
        golf1.setName(nouvGolf);
        golfRepository.save(golf1);
        model.addAttribute("golfs", golfRepository.findAll());

        return "redirect:/golfs";
    }

    @RequestMapping(value = "/golfdelete/{id}", method = RequestMethod.GET)
    public String golfsAdd(Model model, @PathVariable("id") long idGolf) {
        Golf golf1 = golfRepository.findById(idGolf).get();
        golfRepository.delete(golf1);

        return "redirect:/golfs";
    }

    @RequestMapping(value = "/golf/{id}", method = RequestMethod.GET)
    public String golfDetail(Model model, @PathVariable("id") Long id) {
        Golf golf1 = golfRepository.findById(id).get();

        if (golf1.getCourses() != null) {
            for (Course course : golf1.getCourses()) {
                int temps = 0;
                int num = 1;


                for (Hole hole : course.getHoles()) {
                    hole.setNum(Integer.parseInt(String.format("%02d", num)));
                    int par = hole.getPar();
                    if (par == 3 && "3".equals("3")) {
                        temps += 11;
                    } else if (par == 4 && "3".equals("3")) {
                        temps += 14;
                    } else if (par == 5 && "3".equals("3")) {
                        temps += 17;
                    } else if (par == 3) {
                        temps += 9;
                    } else if (par == 4) {
                        temps += 12;
                    } else {
                        temps += 14;
                    }
                    int heures = temps / 60;
                    int minutes = temps % 60;
                    hole.setWalkTime((String.format("%02d:%02d", heures, minutes)));

                    num++;
                }
            }
        }
        golfRepository.save(golf1);

        model.addAttribute("golf", golf1);

        model.addAttribute("courses", courseRepository.findAllByGolf(golf1));

        return "golf";
    }

    @RequestMapping(value = "/golfHoraire/{id}", method = RequestMethod.GET)
    public String golfHoraire(Model model, @PathVariable("id") Long id,
                              @RequestParam(value = "horaire") String horaire,
                              @RequestParam(value = "courseId") long courseId) {
        Golf golf1 = golfRepository.findById(id).get();

        for (Course course : golf1.getCourses()) {
            int temps = 0;
            int num = 1;

            int heures = 0;
            int minutes = 0;
            String horaireSplit[] = {"", ""};
            int horaireInt2 = 0;
            int horaireInt = 0;

            for (Hole hole : course.getHoles()) {


                hole.setNum(Integer.parseInt(String.format("%02d", num)));
                int par = hole.getPar();
                if (par == 3 && "3".equals("3")) {
                    temps += 11;
                } else if (par == 4 && "3".equals("3")) {
                    temps += 14;
                } else if (par == 5 && "3".equals("3")) {
                    temps += 17;
                } else if (par == 3) {
                    temps += 9;
                } else if (par == 4) {
                    temps += 12;
                } else {
                    temps += 14;
                }
                heures = temps / 60;
                minutes = temps % 60;
                if ((horaire == null) || (horaire.equalsIgnoreCase(""))) {
                    horaire = "00:00";
                }
                if (course.getId() == courseId) {
                    horaireSplit = (horaire.split(":"));
                    horaireInt = Integer.parseInt(horaireSplit[0]);
                     horaireInt2 = Integer.parseInt(horaireSplit[1]);
                    horaireInt = (horaireInt + heures) % 24+horaireInt2/60;
                    horaireInt2 = (horaireInt2 + minutes) % 60;
                    hole.setHoraire((String.format("%02d:%02d", horaireInt, horaireInt2)));
                }
                hole.setWalkTime((String.format("%02d:%02d", heures, minutes)));

                num++;
            }
        }
        model.addAttribute("golf", golf1);

        model.addAttribute("courses", courseRepository.findAllByGolf(golf1));
        golfRepository.save(golf1);

        return "golf";
    }

    @RequestMapping(value = "/golf/{id}", method = RequestMethod.POST)
    public String modifyGolf(Model model, @PathVariable("id") Long id, @RequestParam("name") String name) {

        Golf golf1 = golfRepository.findById(id).get();
        golf1.setName(name);

        List<Course> courses = courseRepository.findAllByGolf(golf1);
        model.addAttribute("courses", courses);
        model.addAttribute("golf", golf1);
        courseRepository.saveAll(courses);
        golfRepository.save(golf1);


        return "golfEdit";
    }

    @RequestMapping(value = "golfEdit/{id}", method = RequestMethod.GET)
    public String EditGolf(Model model, @PathVariable("id") Long id) {

        Golf golf1 = golfRepository.findById(id).get();
        golf1.getName();


        model.addAttribute("courses", courseRepository.findAllByGolf(golf1));
        model.addAttribute("golf", golf1);

        courseRepository.saveAll(courseRepository.findAllByGolf(golf1));
        golfRepository.save(golf1);

        return "golfEdit";
    }
    @RequestMapping(value = "retourGolfs", method = RequestMethod.GET)
    public String RetourGolf(Model model) {
        model.addAttribute("golfs",golfRepository.findAll());

        return "golfs";
    }
    @RequestMapping(value = "retourSommaire", method = RequestMethod.GET)
    public String RetourSommaire(Model model) {

        return "index";
    }

}
