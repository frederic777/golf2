package fr.greta94.golf.controllers;

import fr.greta94.golf.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class HoleController {

    @Autowired
    HoleRepository holeRepository;
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    GolfRepository golfRepository;

    @RequestMapping(value = "/hole/", method = RequestMethod.POST)
    public void trouPar(Model model, @RequestParam("par") List<String> pars) {
        Hole par = (Hole) model.getAttribute("par");
        Course course = (Course) model.getAttribute("parcours");

        model.addAttribute("hole", holeRepository.findAllByCourse(course));
        model.addAttribute("course", course);
    }

    @RequestMapping(value = "/holeAdd/{id}", method = RequestMethod.POST)
    public String trouAdd(@PathVariable("id") long id, Model model, @RequestParam("pars") List<String> pars,
                          @RequestParam("parcoursId") Long parcoursId) {
        Course course = courseRepository.getOne(parcoursId);
        Optional<Golf> golf = golfRepository.findById(id);
        course.setGolf(golf.get());
        int i = 1;
        String horaire = "00:00";
            List<Hole> allHoles = new ArrayList<>();
            holeRepository.deleteAll( course.getHoles());
        if ((pars != null) && (i < 19)) {
            for (String par : pars) {
                int part = Integer.parseInt(par);
                Hole hole = new Hole(part, i);

                hole.setCourse(course);
//                holeRepository.save(hole);
//                course.addHoles(hole);

                allHoles.add(hole);

                i++;
            }
        holeRepository.saveAll(allHoles);
            course.setHoles(allHoles);

        }
       course.setGolf(golf.get());
        courseRepository.save(course);
        golf.get().addCourses(course);
        golfRepository.save(golf.get());


        List<Course> courses = courseRepository.findAllByGolf(golf.get());
        int temps = 0;
        int num = 1;

        int heures = 0;
        int minutes = 0;
        String horaireSplit[] = {"", ""};
        int horaireInt2 = 0;
        int horaireInt = 0;

        for (Hole hole : course.getHoles()) {


            hole.setNum(Integer.parseInt(String.format("%02d", num)));
            int par = hole.getPar();
            if (par == 3 && "3".equals("3")) {
                temps += 11;
            } else if (par == 4 && "3".equals("3")) {
                temps += 14;
            } else if (par == 5 && "3".equals("3")) {
                temps += 17;
            } else if (par == 3) {
                temps += 9;
            } else if (par == 4) {
                temps += 12;
            } else {
                temps += 14;
            }
            heures = temps / 60;
            minutes = temps % 60;
            if ((horaire == null) || (horaire.equalsIgnoreCase(""))) {
                horaire = "00:00";
            }
            if (course.getId() == parcoursId) {
                horaireSplit = (horaire.split(":"));
                horaireInt = Integer.parseInt(horaireSplit[0]);
                horaireInt2 = Integer.parseInt(horaireSplit[1]);
                horaireInt = (horaireInt + heures) % 24 + horaireInt2 / 60;
                horaireInt2 = (horaireInt2 + minutes) % 60;
                hole.setHoraire((String.format("%02d:%02d", horaireInt, horaireInt2)));
            }
            hole.setWalkTime((String.format("%02d:%02d", heures, minutes)));

            num++;
        }

        course.setGolf(golf.get());
        courseRepository.save(course);
        golf.get().addCourses(course);
        golfRepository.save(golf.get());

        model.addAttribute("holes", holeRepository.findAllByCourse(course));
        model.addAttribute("course", course);
        model.addAttribute("courses", courses);
        model.addAttribute("golf", golf.get());
        model.addAttribute("pars", pars);
        return "golf";
    }


    @RequestMapping(value = "/holeAdd/{id}", method = RequestMethod.GET)
    public String trouAddGet(@PathVariable("id") long id, Model model, @RequestParam("pars") List<String> pars,
                             @RequestParam("parcoursId") Long parcoursId) {
        Optional<Golf> golf = golfRepository.findById(id);
        List<Course> courses = courseRepository.findAllByGolf(golf.get());
        Course course = courseRepository.getOne(parcoursId);
        course.setGolf(golfRepository.getOne(id));
        courseRepository.save(course);
        golfRepository.save(golf.get());
        model.addAttribute("holes", holeRepository.findAllByCourse(course));
        model.addAttribute("courses", courses);
        model.addAttribute("golf", golf.get());
        return "golf";
    }
}

