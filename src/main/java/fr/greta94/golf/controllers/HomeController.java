package fr.greta94.golf.controllers;

import fr.greta94.golf.models.Course;
import fr.greta94.golf.models.CourseCompose;
import fr.greta94.golf.models.CourseRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
    private CourseRepository courseRepository;
    @RequestMapping("/")
    public String home() {
//        CourseCompose cc=new CourseCompose();
//        cc.setName("Jaune-violet");
//        Course c1= courseRepository.findById(1l).get();
//        Course c2= courseRepository.findById(2l).get();
//
        return "index";
    }

    @RequestMapping("/demo")
    public String demo() { return "demo"; }
}
