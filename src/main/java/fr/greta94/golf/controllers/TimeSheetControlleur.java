package fr.greta94.golf.controllers;

import fr.greta94.golf.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class TimeSheetControlleur {
    @Autowired
    TimeSheetRepository timeSheetRepository;
    @Autowired
    GolfRepository golfRepository;
    @Autowired
    CourseRepository courseRepositry;
   @Autowired
   HoleRepository holeRepository;
    @RequestMapping("/timesheet")
    public String timeSheet(Model model) {
        List<TimeSheet> timeSheet = timeSheetRepository.findAll();
        for (TimeSheet sheet : timeSheet) {
        }
        return "ts";
    }

    @RequestMapping("/depart")
    public String depart(Model model) {
        List<Golf> golfs=golfRepository.findAll();
        List<Course> courses = courseRepositry.findAll();
        model.addAttribute("courses",courses);
        model.addAttribute("golfs",golfs);
        return "nouvelleFeuille";
    }
    @RequestMapping(value="/feuille",method = RequestMethod.GET)
    public String feuille(Model model,@RequestParam("courseId") long parcours, @RequestParam("horaire") String horaire) {

        Course course1=courseRepositry.getOne(parcours);
         // Golf golf = golfRepository.getOne(golfId);
            int temps = 0;
            int num = 1;
            int heures = 0;
            int minutes = 0;

            String horaireSplit[]={"",""};
            int horaireInt2=0;
            int horaireInt=0;
        if (course1.getHoles()!=null){
                for (Hole hole : course1.getHoles()) {


                    hole.setNum(Integer.parseInt(String.format("%02d", num)));
                    int par = hole.getPar();
                    if (par == 3 && "3".equals("3")) {
                        temps += 11;
                    } else if (par == 4 && "3".equals("3")) {
                        temps += 14;
                    } else if (par == 5 && "3".equals("3")) {
                        temps += 17;
                    } else if (par == 3) {
                        temps += 9;
                    } else if (par == 4) {
                        temps += 12;
                    } else {
                        temps += 14;
                    }
                    heures = temps / 24;
                    minutes = temps % 60;
                if ((horaire == null) ||(horaire.equalsIgnoreCase("")))  {
                    horaire = "00:00";
                }
                if(course1.getId() == parcours){
                    horaireSplit = (horaire.split(":"));
                    horaireInt = Integer.parseInt(horaireSplit[0]);
                     horaireInt2 = Integer.parseInt(horaireSplit[1]);
                    horaireInt2 =(horaireInt2+ minutes)%60;
                    horaireInt = (horaireInt + heures) % 24+(horaireInt2)/60;
                    hole.setHoraire((String.format("%02d:%02d", horaireInt, horaireInt2)));
                }
                    hole.setWalkTime((String.format("%02d:%02d", heures, minutes)));

                    num++;
                }
            }

        model.addAttribute("course",  course1);
        //model.addAttribute("golf",  golf);
        return "parcours";
    }
    @RequestMapping(value="/timesheetSchedule",method = RequestMethod.GET)
    public String printCourse(Model model,@RequestParam Long courseId,
                             @RequestParam List<String> pars)
    {
    Course course=(courseRepositry.getOne(courseId));
    int num=1;
    int temps=0;
    int heures=0;
    int minutes=0;

        List<Hole> allHole = new ArrayList<>();
        for (Hole hole : course.getHoles()) {


        hole.setNum(Integer.parseInt(String.format("%02d", num)));
        int par = hole.getPar();
        if (par == 3 && "3".equals("3")) {
            temps += 11;
        } else if (par == 4 && "3".equals("3")) {
            temps += 14;
        } else if (par == 5 && "3".equals("3")) {
            temps += 17;
        } else if (par == 3) {
            temps += 9;
        } else if (par == 4) {
            temps += 12;
        } else {
            temps += 14;
        }
        heures = temps / 60;
        minutes = temps % 60;
           hole.setWalkTime((String.format("%02d:%02d", heures, minutes)));
        hole.setCourse(course);
       allHole.add(hole);
        num++;}
        course.setHoles(allHole);
        courseRepositry.save(course);
         model.addAttribute(course);
    return "parcours";
    }
}


