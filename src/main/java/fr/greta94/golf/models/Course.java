package fr.greta94.golf.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToMany(mappedBy = "course", fetch =FetchType.EAGER)
    private List<Hole> holes;

    @ManyToOne
    @JoinColumn(name="golf_id", referencedColumnName = "id")
    private Golf golf;

    public Course() {
    }

    public Course(String name) {
        this.name = name;
        this.holes=new ArrayList<Hole>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Hole> getHoles() {
        if (holes == null)
            return new ArrayList<>();
        return holes;
    }

    public void setHoles(List<Hole> holes) {
        this.holes = holes;
    }

    public void addHoles(Hole hole) {
        this.holes.add(hole);
    }

    public Golf getGolf() {
        return golf;
    }

    public void setGolf(Golf golf) {
        this.golf = golf;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course that = (Course) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(holes, that.holes) &&
                Objects.equals(golf, that.golf);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, holes, golf);
    }
}
