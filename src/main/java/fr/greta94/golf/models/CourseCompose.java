package fr.greta94.golf.models;

import javax.persistence.*;
import java.util.List;
@Entity

public class CourseCompose {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    private int walkingTimeBetweenCourse;
    @ManyToMany
    private List<Course> courses;
    @ManyToOne
    @JoinColumn(name="golf_id", referencedColumnName = "id")
    private Golf golf;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWalkingTimeBetweenCourse() {
        return walkingTimeBetweenCourse;
    }

    public void setWalkingTimeBetweenCourse(int walkingTimeBetweenCourse9) {
        this.walkingTimeBetweenCourse = walkingTimeBetweenCourse9;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public Golf getGolf() {
        return golf;
    }

    public void setGolf(Golf golf) {
        this.golf = golf;
    }
}
