package fr.greta94.golf.models;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseComposeRepository extends JpaRepository<CourseCompose, Long> {
}
