package fr.greta94.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="Editeur")
public class Editeur extends User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String commentaire;
    private int idUser;

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Editeur editeur = (Editeur) o;
        return id == editeur.id &&
                idUser == editeur.idUser &&
                Objects.equals(commentaire, editeur.commentaire);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, commentaire, idUser);
    }
}
