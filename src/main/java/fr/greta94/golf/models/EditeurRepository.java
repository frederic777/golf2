package fr.greta94.models;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EditeurRepository extends JpaRepository <Editeur,Integer>{

}
