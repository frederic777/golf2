package fr.greta94.golf.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "golfs")
@SequenceGenerator(name="sequencegolfs",initialValue = 6)
public class Golf {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
     private Long id;
    @NotNull
    private String name;
    @OneToMany(mappedBy = "golf", fetch = FetchType.EAGER,cascade = CascadeType.DETACH)
    private List<Course> courses;
    @OneToMany(mappedBy = "golf", fetch = FetchType.LAZY,cascade = CascadeType.DETACH)
    private List<CourseCompose> courseComposes;

    public Golf() {
        this.courses = new ArrayList<>();
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public List<CourseCompose> getCourseComposes() {
        return courseComposes;
    }

    public void setCourseComposes(List<CourseCompose> courseComposes) {
        this.courseComposes = courseComposes;
    }

    public Golf(@NotNull String name) {
        this.courses = new ArrayList<>();
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void addCourses(Course course) {
        this.courses.add(course);
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Course> getCourses() {
        return courses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Golf that = (Golf) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(courses, that.courses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, courses);
    }
}
