package fr.greta94.golf.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import org.springframework.data.annotation.Transient;
@Entity
@Table(name = "holes")
public class Hole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @NotNull
    private int par;
    @NotNull
    private int num;
    private String walkTime;
    @Transient
    private String horaire="00:00";
    @ManyToOne
    @JoinColumn(name="course_id", referencedColumnName = "id")
    private Course course;
    @Transient
    private String heure;
    public Hole() {

    }

    public Hole(@NotNull int par, @NotNull int num) {
        this.par = par;
        this.num = num;
        this.horaire="00:00";
    }

    public String getHoraire() {
        return horaire;
    }

    public void setHoraire(String horaire) {
        this.horaire = horaire;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPar() {
        return par;
    }

    public void setPar(int par) {
        this.par = par;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getWalkTime() { return walkTime;
    }

    public void setWalkTime(String walkTime) {
        int num = 1;
        int temps = 0;
        this.walkTime=walkTime;

    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hole that = (Hole) o;
        return par == that.par &&
                num == that.num &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(course, that.course);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, par, num, course);
    }
}
