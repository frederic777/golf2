package fr.greta94.golf.models;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HoleRepository extends JpaRepository<Hole, Long> {
    List<Hole> findAllByCourse(Course course);
}
