package fr.greta94.models;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="RegleLocal")
public class ReglesLocal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nom;
    private String contenu;
    @ManyToMany(mappedBy = "users")
    private List<User> users;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReglesLocal that = (ReglesLocal) o;
        return id == that.id &&
                Objects.equals(nom, that.nom) &&
                Objects.equals(contenu, that.contenu) &&
                Objects.equals(users, that.users);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, contenu, users);
    }
}
