package fr.greta94.models;
import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="Rubrique")
public class Rubrique {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nom;
    @ManyToMany(mappedBy = "users")
    private List<User> users;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rubrique rubrique = (Rubrique) o;
        return id == rubrique.id &&
                Objects.equals(nom, rubrique.nom) &&
                Objects.equals(users, rubrique.users);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, users);
    }
}
