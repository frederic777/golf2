package fr.greta94.models;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RubriqueRepository extends JpaRepository <Rubrique,Integer>{

}
