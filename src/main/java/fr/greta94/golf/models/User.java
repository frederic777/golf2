package fr.greta94.models;
import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="User")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nom;
    private String prenom;
    private String coordonnees;
    @ManyToMany
    private List<Rubrique> rubriques;
    @ManyToMany
    private List<ReglesLocal> reglesLocals;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getCoordonnees() {
        return coordonnees;
    }

    public void setCoordonnees(String coordonnees) {
        this.coordonnees = coordonnees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(nom, user.nom) &&
                Objects.equals(prenom, user.prenom) &&
                Objects.equals(coordonnees, user.coordonnees) &&
                Objects.equals(rubriques, user.rubriques) &&
                Objects.equals(reglesLocals, user.reglesLocals);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom, coordonnees, rubriques, reglesLocals);
    }
}


